import React from 'react'
import {Carousel} from 'react-bootstrap'

export default function Reviews() {
    return (
        <div className='reviews-container'>
        <Carousel className='text-center review'>
            <Carousel.Item>
                <div>
                <h3 style={{color: '#0d5077'}}>They Say</h3>
                <p className='caption'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.<br />
                    <span style={{fontFamily: 'Source Sans Pro'}}>
                        Man Wol<br />
                        <small>Teacher</small>
                    </span>
                </p>
                </div>
            </Carousel.Item>
            <Carousel.Item>
                <div>
                <h3>They Say</h3>
                <p className='caption'>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<br />
                    <span style={{fontFamily: 'Source Sans Pro'}}>
                        Eun Chan Song<br />
                        <small>Teacher</small>
                    </span>
                </p>
                </div>
            </Carousel.Item>
            <Carousel.Item>
                <div>
                <h3>They Say</h3>
                <p className='caption'>Praesent commodo cursus magna, vel scelerisque nisl consectetur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br />
                    <span style={{fontFamily: 'Source Sans Pro'}}>
                        Kim Soo Hyun<br />
                        <small>Teacher</small>
                    </span>
                </p>
                </div>
            </Carousel.Item>
        </Carousel>
        <br />
        </div>
    )
}
