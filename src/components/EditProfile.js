import React, { useState} from 'react'
import {Modal }from 'react-bootstrap'
import ChangePassword from './ChangePassword'
import Footer from './Footer'

export default function Profile() {


    //Modal
    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const [name] = useState(['Monica'])

  return (
    <>

    <div className='container profile-container'>
      <br />
      <p className='text-center'>MY PROFILE</p>
      <div>

      <h3 className='text-center initials'>{name.map((initial) => initial[0]).join('')}</h3>
      <br />

      <div className='d-flex'>
        <label className='mr-2'>
          <p>Username</p>
        </label>
        <input
          className="form-control ml-2 rounded-0"
          type="text"
          placeholder={name}
          />
      </div>
      <br />

      <div className='d-flex'>
        <label className='mr-2'>
          <p>Email</p>
        </label>
        <input
          className="form-control ml-5 rounded-0"
          type="email"
          placeholder="monicaoduor2@gmail.com"
          />
      </div>
      <br />

      <div className='d-flex'>
        <div className='mr-4'>
          <p>Password</p> </div>
        <button className= 'password btn btn-sm' onClick={handleShow} >
          <i className="fas fa-lock"></i>&nbsp;
            Click to edit
        </button>
        <Modal centered show={show} onHide={handleClose}>
          <ChangePassword />
        </Modal>
      </div>
      <br />

      <br />

      <button className='sign-out btn btn-md btn-dark'>
        SAVE PROFILE
      </button>
      </div>
      <hr />
            <Footer />

    </div>
    </>
  )
}
