import React from 'react'
import Reviews from './Reviews'
import Footer from './Footer'
import globeEarth from './icons/globeEarth.png'
import customerSupport from './icons/customerSupport.png'
import repeat from './icons/repeat.png'

export default function About() {
    return (
        <div className='container'>

            <div className='text-center mt-5'>
                <p>ABOUT US</p>
                <h2 style={{borderBottom: '1px solid #b90163'}}>exambot</h2>
            </div>
            <br />
            <h3>
                Specializing in developing high-quality examinations.
            </h3>
            <p>
                Proin ac lobortis arcu, a vestibulum augue. Vivamus ipsum neque, facilisis vel mollis vitae, mollis nec ante. Quisque aliquam dictum condim.
                Phasellus vehic sagittis euismod. Lorem ipsum dolor sit ametcon, sectetur adipiscing elit.
            </p>
            <br />
            <div className='icons row'>
                <div className='col-md-4 text-center'>
                    <img src={globeEarth} alt='world-icon' />
                    <br/>
                        <small
                            className='text-center'
                            style={{color: '#0d5077'}}>
                                WORLWIDE SERVICE
                        </small>
                    <br /><br />
                    <p>Lorem ipsum dolor sit ametcon, sectetur adipiscing elit. Phasellus vehic sagittis euismod.</p>
                </div>

                <div className='col-md-4 text-center'>
                    <img src={repeat}alt='repeat-icon'/>
                    <br />
                        <small
                            className='text-center'
                            style={{color: '#0d5077'}}>
                                REDUCE REPETIVENESS
                        </small>
                    <br /><br />
                    <p>Reduce monotonous tasks and minimise the amount of time spent on repetitive activities.</p>
                </div>

                <div className='col-md-4 text-center'>
                    <img src={customerSupport} alt='customer-care-icon'/>
                    <br />
                        <small
                        className='text-center'
                        style={{color: '#0d5077'}}>
                            CUSTOMER CARE
                        </small>
                    <br /><br />
                    <p>Phasellus vehic sagittis euismod. Lorem ipsum dolor sit ametcon, sectetur adipiscing elit.</p>
                </div>

            </div>
            <br />

            <Reviews />

            <hr />
            <Footer />
            <br />
        </div>
    )
}
