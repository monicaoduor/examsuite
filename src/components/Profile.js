import React from 'react'
import {Table} from 'react-bootstrap'
import home from './icons/home.png'
import customerSupport from './icons/customerSupport.png'
import pen from './icons/pen.png'
import padlock from './icons/padlock.png'
import settings from './icons/settings.png'

export default function Profile() {
    return (
        <div className = 'container mt-3'>
            <div className="sidebar">

                    <a className="active " href="#home">
                        <img alt='icon'src={home} />
                        &nbsp;Account Overview&nbsp;
                    </a>

                <a href="#news" className='hide'>
                    <img alt='icon'src={pen}/>
                    &nbsp;Edit Profile
                </a>
                <a href="#contact" className='hide'>
                    <img alt='icon'src={padlock}/>
                    &nbsp;Edit password
                </a>
                <a href="#about" className='hide'>
                    <img alt='icon'src={settings}/>
                    &nbsp;Settings
                </a>
            </div>
        <div className="content">
            <br />
            <h1 style={{fontWeight: '900'}}>Account Overview</h1>
            <h3 style={{fontWeight: '700'}}>Profile</h3>

            <Table borderless hover style={{lineHeight: '40px'}}>
                <tbody hover>
                    <tr className='border-bottom'>
                    <td className='text-secondary' style={{textAlign: 'left'}}>Username</td>
                    <td style={{textAlign: 'right'}}>MarkOtto</td>
                    </tr>

                    <tr className='border-bottom'>
                    <td className='text-secondary' style={{textAlign: 'left'}}>Email</td>
                    <td  style={{textAlign: 'right'}}>markotto@gmail.com</td>
                    </tr>
                </tbody>
            </Table>

            <div className='row'>
                <button className='btn border-dark col-5'>Edit profile</button>
                <p className='col-2'></p>
                <button className='btn btn-dark col-5'>Sign out</button>
            </div>
            </div>
        </div>
    )
}
