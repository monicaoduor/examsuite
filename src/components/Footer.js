import React from 'react'

export default function Footer() {
    return (
        <div className='text-center'>
                <p className='text-center m-0'>
                    Copyright © exambot {new Date().getFullYear()}
                    &nbsp;
                </p><br />
                <div className='m-3'>
                    <i className="fab p-2 fa-facebook-f"></i>
                    <i className="fab p-2 fa-twitter"></i>
                    <i className="fab p-2 fa-instagram"></i>
                    <i className="fab p-2 fa-linkedin-in"></i>
                </div>
            </div>

    )
}
