import React from 'react'

export default function ChangePassword() {
    return (
        <div className='p-4 container'>
            <h4 className='m-2'>Change Password</h4>
            <small>Current Password</small>
                <input
                    type='password'
                    className='form-control rounded-0'
                    placeholder='New Password'
                    />
            <small>New Password</small>
                <input
                    type='password'
                    className='form-control rounded-0'
                    placeholder='New Password'
                    />
            <small>Confirm Password</small>
                <input
                    type='password'
                    className='form-control rounded-0'
                    placeholder='Confirm Password'
                    />
                     <br />
            <button
                className='btn  btn-lg btn-dark rounded-0 btn-sm mt-2'>
                Update password
            </button>
        </div>
    )
}
