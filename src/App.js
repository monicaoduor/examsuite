import React from 'react';
import './App.css';
import Profile from './components/Profile';
import About from './components/About';
import ChangePassword from './components/ChangePassword';
import EditProfile from './components/EditProfile';
import Reviews from './components/Reviews';
function App() {
  return (
    <div>

      <About />
      <ChangePassword />
      <EditProfile />
      <Reviews />


    </div>
  )
}

export default App;
